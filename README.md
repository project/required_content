# Required Content

The purpose of this module is to ensure any content that is required by
configuration exists when that configuration is loaded.

## Motivation

It can be hard to coordinate the deployment of both content and configuration
changes together.

A typical scenario:
1. A site's source code - along with its exported configuration - is stored
   in version control.
2. A developer wants to add a custom block to a page. They do this locally,
   export the configuration and commit the changes.
3. The configuration refers to a block with a UUID. This block only exists
   on the developer's instance.
4. The new configuration is deployed, but the block needs to be created
   separately with that same UUID.

This module aims to solve that.

## How does this module work?

Go to _Administration_, _System_, _Required content_. For each content
dependency, you can dictate what should happen if that dependency is
missing. You can specify whether to create a missing entity automatically
or not, and any properties a newly created entity should have.
