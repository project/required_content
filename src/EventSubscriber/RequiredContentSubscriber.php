<?php

namespace Drupal\required_content\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\Importer\MissingContentEvent;
use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Acts on missing content events.
 */
class RequiredContentSubscriber implements EventSubscriberInterface {

  protected MessengerInterface $messenger;

  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Handles the missing content event.
   *
   * Here we examine each of the content UUIDs that are reported as missing.
   * If a UUID is one we know about, create the content entity.
   */
  public function onMissingContent(MissingContentEvent $event) {
    foreach ($event->getMissingContent() as $uuid => $data) {
      $entityTypeId = $data['entity_type'];
      $bundle = $data['bundle'];
      if ($this->handleMissingContentEntity($entityTypeId, $bundle, $uuid)) {
        $event->resolveMissingContent($uuid);
      }
    }
  }

  function handleMissingContentEntity(string $entityTypeId, string $bundle, string $uuid) {
    $config = \Drupal::config('required_content.settings');
    if ($config) {
      $entities = $config->get('entities');
      if (isset($entities[$entityTypeId][$uuid])) {
        $entityType = \Drupal::entityTypeManager()->getDefinition($entityTypeId);

        $values = $entities[$entityTypeId][$uuid];
        $values[$entityType->getKey('bundle')] = $bundle;
        $values['uuid'] = $uuid;

        $entity = \Drupal::entityTypeManager()->getStorage($entityTypeId)->create($values);
        $entity->save();

        /** @var LoggerInterface $logger */
        $logger = \Drupal::service('logger.channel.default');
        $logger->notice("Entity was created: $entityTypeId / $uuid");

        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::IMPORT_MISSING_CONTENT => ['onMissingContent']
    ];
  }

}
