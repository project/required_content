<?php

namespace Drupal\required_content\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\Importer\MissingContentEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MissingContentSubscriber implements EventSubscriberInterface {

  protected MessengerInterface $messenger;
  protected EntityTypeManagerInterface $entityTypeManager;


  public function __construct(MessengerInterface $messenger, EntityTypeManagerInterface $entityTypeManager) {
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
  }


  /**
   * Handles the missing content event.
   *
   * Here we examine each of the content UUIDs that are reported as missing.
   * If a UUID is one we know about, create the content entity.
   *
   * @param \Drupal\Core\Config\Importer\MissingContentEvent $event
   *   The missing content event.
   */
  public function onMissingContent(MissingContentEvent $event) {
    foreach ($event->getMissingContent() as $uuid => $data) {
      $entityType = $data['entity_type'];
      $bundle = $data['bundle'];
      if ($this->handleMissingContentEntity($entityType, $bundle, $uuid)) {
        $event->resolveMissingContent($uuid);
      }
    }
  }

  /**
   * Handle a single missing content entity.
   *
   * If the UUID is something we know about, we can create an
   * entity programmatically here.
   *
   * @return bool
   *   TRUE if we created the entity, FALSE if we ignored it.
   */
  protected function handleMissingContentEntity(string $entityType, string $bundle, string $uuid): bool {
    // This is an example of creating a content entity upon demand.
    // A custom block with a copyright message has been created and
    // the placement of that block exported to configuration.
    // The configuration will refer to a content entity's UUID.
    // We create that entity - with the correct UUID - here.
    /*
    if ($entityType === "block_content" && $uuid === '2308f49a-e6ab-449e-b21e-92342c933c70') {
      $blockContent = $this->entityTypeManager->getStorage($entityType)->create([
        'type' => $bundle,
        'uuid' => $uuid,
        'info' => 'Copyright',
      ]);
      $blockContent->save();
      return TRUE;
    }
    */
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ConfigEvents::IMPORT_MISSING_CONTENT][] = ['onMissingContent'];
    return $events;
  }

}
