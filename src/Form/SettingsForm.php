<?php

namespace Drupal\required_content\Form;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for required content module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'required_content_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['required_content.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('required_content.settings');
    $rulesByEntityTypeAndUuid = $settings->get('entities') ?? [];

    /** @var ConfigManagerInterface $configManager */
    $configManager = \Drupal::service('config.manager');

    $f = $configManager->getConfigFactory();


    $contentDependencies = [];

    foreach ($f->listAll() as $name) {
      $c = $f->get($name);
      $data = $c->getRawData();

      $contentDependencyKeys = [];
      if (isset($data['dependencies']['content'])) {
        $contentDependencyKeys = array_merge($contentDependencyKeys, $data['dependencies']['content']);
      }
      if (isset($data['dependencies']['enforced']['content'])) {
        $contentDependencyKeys = array_merge($contentDependencyKeys, $data['dependencies']['enforced']['content']);
      }

      foreach (array_unique($contentDependencyKeys) as $key) {
        $contentDependencies[$key][] = $name;
      }
    }

    $introText1 = $this->formatPlural(count($contentDependencies),
      'There is one content dependency in this site\'s configuration.',
      'There are @count content dependencies in this site\'s configuration.',
    );
    $introText2 = $this->t('Use this form to dictate how missing dependencies will be handled upon a configuration import.');

    $form['intro'] = [
      '#markup' => '<p>' . Html::escape($introText1) . '</p><p>' . Html::escape($introText2) . '</p>',
    ];

    $form['items'] = [
      '#tree' => TRUE,
    ];


    foreach ($contentDependencies as $key => $usage) {
      [$entityTypeId, $bundle, $uuid] = explode(':', $key);

      /** @var ContentEntityTypeInterface $entityType */
      $entityType = \Drupal::entityTypeManager()->getDefinition($entityTypeId);
      $bundleType = \Drupal::entityTypeManager()->getStorage($entityType->getBundleEntityType())->load($bundle);

      /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository */
      $entityRepository = \Drupal::service('entity.repository');
      $entity = $entityRepository->loadEntityByUuid($entityTypeId, $uuid);

      /** @var string|FALSE $labelKey */
      $labelKey = $entityType->getKey('label');

      // The presence of an entityType/uuid combination in the settings indicates
      // that this module should create that missing entity.
      $createEntity = array_key_exists($entityTypeId, $rulesByEntityTypeAndUuid)
        && array_key_exists($uuid, $rulesByEntityTypeAndUuid[$entityTypeId]);

      $rules = $createEntity ? $rulesByEntityTypeAndUuid[$entityTypeId][$uuid] : [];

      $label = '';
      $properties = $rules;

      // The label is just another property, defined by $labelKey,
      // but we prefer to handle it using a dedicated text field.
      // Remove it from the properties before presenting the UI.
      if ($labelKey) {
        $label = $rules[$labelKey] ?? '';
        unset($properties[$labelKey]);
      }

      // If there are no rules for this entity type/uuid combination,
      // but such an entity exists, suggest some default values.
      if ($entity && !$createEntity) {
        $label = $entity->label();
        // TODO: include values for base fields that do not have a default value
        // TODO: include values for base fields where the actual value is different from the default
        if ($entityType->hasKey('published')) {
          $publishedKey = $entityType->getKey('published');
          $properties[$publishedKey] = (bool) $entity->get($publishedKey)->value;
        }
      }



      $form['items'][$key] = [
        '#type' => 'fieldset',
        '#title' => $entity
          ? "{$entityType->getLabel()}: <em>{$entity->label()}</em>"
          : "{$entityType->getLabel()} (missing)",
        '#tree' => TRUE,
      ];

      $form['items'][$key]['usage'] = [
        '#type' => 'item',
        '#title' => 'Used in configuration',
        'value' => [
          '#plain_text' => implode(', ', $usage),
        ],
      ];

      $form['items'][$key]['bundle'] = [
        '#type' => 'item',
        '#title' => $entityType->getBundleLabel(),
        'value' => [
          '#plain_text' => $bundleType->label(),
        ],
      ];

      $form['items'][$key]['uuid'] = [
        '#type' => 'item',
        '#title' => 'UUID',
        'value' => ['#plain_text' => $uuid],
      ];

      $form['items'][$key]['create'] = [
        '#type' => 'radios',
        '#title' => 'If this entity is missing',
        '#options' => [
          '0' => 'Do nothing',
          '1' => 'Create a new entity automatically upon config import',
        ],
        '#default_value' => $createEntity ? '1' : '0',
      ];

      $form['items'][$key]['rules'] = [
        '#type' => 'container',
      ];

      if ($labelKey) {
        /** @var EntityFieldManagerInterface $entityFieldManager */
        $entityFieldManager = \Drupal::service('entity_field.manager');
        $fieldDefinitions = $entityFieldManager->getBaseFieldDefinitions($entityTypeId);

        $form['items'][$key]['rules']['label'] = [
          '#type' => 'textfield',
          '#title' => $fieldDefinitions[$labelKey]->getLabel(),
          '#required' => $fieldDefinitions[$labelKey]->isRequired(),
          '#default_value' => $label,
        ];
      }

      $form['items'][$key]['rules']['properties'] = [
        '#type' => 'textarea',
        '#title' => 'Additional properties',
        '#description' => 'Properties must be specified in YAML format',
        '#default_value' => $properties ? Yaml::encode($properties) : '',
      ];

      // Sprinkling of JS to make the UI more manageable.
      $selector = ":input[name='items[$key][create]']";
      $form['items'][$key]['rules']['#states'] = [
        'visible' => [
          $selector => ['value' => 1],
        ],
      ];

    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('items') as $key => $values) {

      // Make sure properties, if specified, are a valid YAML array.
      if (!empty($values['rules']['properties'])) {
        try {
          $parsed = Yaml::decode($values['rules']['properties']);
          if (!is_array($parsed)) {
            $form_state->setError($form['items'][$key]['properties'], 'YAML must be an array');
          }
        } catch (InvalidDataTypeException $e) {
          $form_state->setError($form['items'][$key]['properties'], 'Invalid YAML');
        }
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $rulesByEntityTypeAndUuid = [];

    foreach ($form_state->getValue('items') as $key => $values) {
      [$entityTypeId, $bundle, $uuid] = explode(':', $key);

      $create = (bool) $values['create'];
      if (!$create) {
        continue;
      }

      /** @var ContentEntityTypeInterface $entityType */
      $entityType = \Drupal::entityTypeManager()->getDefinition($entityTypeId);
      $labelKey = $entityType->getKey('label');

      if ($create) {
        $propertiesYaml = $values['rules']['properties'] ?? '';
        $properties = $propertiesYaml ? Yaml::decode($propertiesYaml) : [];

        // validateForm() should ensure this is an array, but do an extra
        // check to be safe, rather than saving bad configuration.
        if (!is_array($properties)) {
          throw new \RuntimeException('Properties are expected to be an array in YAML format');
        }

        // Include the label as one of the properties if appropriate.
        $label = $values['rules']['label'] ?? '';
        if ($labelKey && $label) {
          $properties[$labelKey] = $label;
        }
      }

      $rulesByEntityTypeAndUuid[$entityTypeId][$uuid] = $properties;
    }


    // Normalise the configuration.
    // We sort the entities by entity type and then by UUID so that the
    // when this configuration ends up exported to YAML we will have a
    // predictable order.

    ksort($rulesByEntityTypeAndUuid);
    foreach ($rulesByEntityTypeAndUuid as $entityTypeId => $rulesByUuid) {
      ksort($rulesByEntityTypeAndUuid[$entityTypeId]);
      foreach ($rulesByUuid as $uuid => $properties) {
        ksort($rulesByEntityTypeAndUuid[$entityTypeId][$uuid]);
      }
    }


    // Save the configuration

    $this->config('required_content.settings')
      ->set('entities', $rulesByEntityTypeAndUuid)
      ->save();

    parent::submitForm($form, $form_state);
    $form_state->setRebuild(FALSE);
  }

}
